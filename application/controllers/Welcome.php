<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function index()
	{
		$this->load->database();
		$query = $this->db->get('user_backup');

		foreach ($query->result() as $row)
		{
		        echo $row->username;
		}
	}

	public function cekuser($username,$absen){
		$this->load->database();
		$sql = 'SELECT * FROM user_backup where username = "'.$username.'" AND absen = '.$absen;
     	$query = $this->db->query($sql);

		if($query->result() != null){
		
    	$object = $query->result();
    	print_r($object[0]->id."~".$object[0]->username."~".$object[0]->absen."~".$object[0]->eval."~".$object[0]->pd."~".$object[0]->ans."~".$object[0]->score);
    	}else{
    		echo "nouser";
    	}
	}

	public function adduser($username, $absen){
		$this->load->database();

		$data = array(
        	'username' => $username,
        	'absen' => $absen
		);

		$this->db->insert('user_backup', $data);

		$data = array(
        	'initiator' => 1
		);

		$this->db->insert('keg_backup', $data);

	}

	public function nextsoal($id, $nextsoal, $ans, $score){
		$this->load->database();


		$this->db->set('eval', $nextsoal);
		$this->db->set('ans', $ans);
		$this->db->set('score', $score);
		$this->db->where('id', $id);
		$this->db->update('user_backup');

	}


	public function getSoal($nosoal){
	    $this->load->database();
	    
	    $sql = 'SELECT * FROM eval_backup where id = '.$nosoal;
     	$query = $this->db->query($sql);

		if($query->result() != null){
		
    	$object = $query->result();
    	print_r($object[0]->id."~".$object[0]->qu1."~".$object[0]->img."~".$object[0]->qu2."~".$object[0]->a."~".$object[0]->b."~".$object[0]->c."~".$object[0]->d."~".$object[0]->ans);
    	}else{
    		echo "nosoal";
    	}
	    
	    
	}

	public function resetSoal($id){

		$this->load->database();

		$this->db->set('eval', 1);
		$this->db->where('id', $id);
		$this->db->update('user_backup');

	}


	public function updatePD($id, $pd){

		$this->load->database();


		$this->db->set('pd', $pd);
		$this->db->where('id', $id);
		$this->db->update('user_backup');

	}

	public function getPostedText($user_id){

		$this->load->database();
	    
	    $sql = 'SELECT * FROM keg_backup where user_id = '.$user_id;
     	$query = $this->db->query($sql);

		if($query->result() != null){
		
    	$object = $query->result();
    	print_r(
    		$object[0]->user_id."~".
    		$object[0]->ak11."~".
    		$object[0]->ak12."~".
    		$object[0]->ak13."~".
    		$object[0]->ak14."~".
    		$object[0]->ak15."~".
    		$object[0]->ak21."~".
    		$object[0]->ak22."~".
    		$object[0]->ak23."~".
    		$object[0]->ak24."~".
    		$object[0]->ak25."~".
    		$object[0]->ak31."~".
    		$object[0]->ak32."~".
    		$object[0]->ak33."~".
    		$object[0]->ak34."~".
    		$object[0]->ak35."~".
    		$object[0]->ak41."~".
    		$object[0]->ak42."~".
    		$object[0]->ak43."~".
    		$object[0]->ak44."~".
    		$object[0]->ak45
    	);
    	}else{
    		echo "notext";
    	}

	}

	public function setPostedText(){

		$user_id = $this->input->post('user_id');
		$ak11 = $this->input->post('ak11');
		$ak12 = $this->input->post('ak12');
		$ak13 = $this->input->post('ak13');
		$ak14 = $this->input->post('ak14');
		$ak15 = $this->input->post('ak15');

		$ak21 = $this->input->post('ak21');
		$ak22 = $this->input->post('ak22');
		$ak23 = $this->input->post('ak23');
		$ak24 = $this->input->post('ak24');
		$ak25 = $this->input->post('ak25');

		$ak31 = $this->input->post('ak31');
		$ak32 = $this->input->post('ak32');
		$ak33 = $this->input->post('ak33');
		$ak34 = $this->input->post('ak34');
		$ak35 = $this->input->post('ak35');

		$ak41 = $this->input->post('ak41');
		$ak42 = $this->input->post('ak42');
		$ak43 = $this->input->post('ak43');
		$ak44 = $this->input->post('ak44');
		$ak45 = $this->input->post('ak45');


		$this->load->database();

		$this->db->set('ak11', $ak11);
		$this->db->set('ak12', $ak12);
		$this->db->set('ak13', $ak13);
		$this->db->set('ak14', $ak14);
		$this->db->set('ak15', $ak15);
		$this->db->set('ak21', $ak21);
		$this->db->set('ak22', $ak22);
		$this->db->set('ak23', $ak23);
		$this->db->set('ak24', $ak24);
		$this->db->set('ak25', $ak25);
		$this->db->set('ak31', $ak31);
		$this->db->set('ak32', $ak32);
		$this->db->set('ak33', $ak33);
		$this->db->set('ak34', $ak34);
		$this->db->set('ak35', $ak35);
		$this->db->set('ak41', $ak41);
		$this->db->set('ak42', $ak42);
		$this->db->set('ak43', $ak43);
		$this->db->set('ak44', $ak44);
		$this->db->set('ak45', $ak45);

		$this->db->set('user_id', $user_id);

		$this->db->where('user_id', $user_id);
		$this->db->update('keg_backup');

	}



}
